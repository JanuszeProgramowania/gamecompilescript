#include <stdio.h>
#include <stdlib.h>
#include "work.h"
#include "gitmanager.h"

#ifdef __WIN32__
	#define WINDOWS 1
#else
	#define WINDOWS 0
#endif

#define MAX_BUFF_LENGHT 1<<14 //8192

int main(int argc, char** argv, char *env[])
{
	if (WINDOWS)
	{
		printf("I am very sorry, but this script is designed to run only on *BSD systems.\n");
		return 4;
	}
	argc--;
	*argv++;
	int option;
	int i;
	char * C_OPTION_PARAM = "version.cpp";
	int IS_R_OPTION = 0;
	char R_OPTION_PARAM[MAX_BUFF_LENGHT];
	int IS_T_OPTION = 0;
	char buff[MAX_BUFF_LENGHT];
	
	int MANUAL_INPUT = 1;
	dl_Init();
	dl_WriteLog("Initialized successfully!");
	dl_WriteLog("Argument count is %d", argc);
	if (argc == 0)
	{
		dl_WriteLog("Wrong syntax. Argument count is 0");
		ShowWrongSyntaxInfo();
		return 1;
	}
	dl_WriteLog("Starting arguments check");
	while(argc--)
	{
		dl_WriteLog("Current argument is %s", *argv);
		option = strcmp("-v", *argv);
		if (option == 0)
		{
			PrintVersion();
			return 0;
		}
		
		option = strcmp("-l", *argv);
		if (option == 0)
		{
			ShowHelp();
			return 0;
		}
		
		option = strcmp("-c", *argv);
		if (option == 0)
		{
			argc--;
			*argv++;
			C_OPTION_PARAM = *argv;
			dl_WriteLog("Target version file to change is %s", *argv);
			continue;
		}
		
		option = strcmp("-t", *argv);
		if (option == 0)
		{
			IS_T_OPTION = 1;
			dl_WriteLog("GameCompileScript will use built-in template");
		}
		
		option = strcmp("-G", *argv);
		if (option == 0)
		{
			dl_WriteLog("Checking if git scm is installed");
			if (GitInstalled())
			{
				LastRevision(R_OPTION_PARAM, MAX_BUFF_LENGHT);
				MANUAL_INPUT--;
				dl_WriteLog("Building last revision format string");
			}
		}
		
		*argv++;
	}
	
	dl_WriteLog("End of arguments check");
	
	if (MANUAL_INPUT)
	{
		printf("Please specify a game revision: ");
		dl_WriteLog("Asking user for manual revision input %s", *argv);
		scanf ("%[^\n]%*c", R_OPTION_PARAM);
		dl_WriteLog("Got %s as revision from user", R_OPTION_PARAM);
	}
	else
		dl_WriteLog("Revision is set to %s", R_OPTION_PARAM);
	
	if (R_OPTION_PARAM != NULL)
		WriteVersionToFile(C_OPTION_PARAM, R_OPTION_PARAM, IS_T_OPTION);
	else
	{
		printf("You have to specify revision to make the script working!\n");
		dl_WriteLog("Error: no revision specified");
		exit(1);
	}
	
	printf("Do You want to compile script right now? [Y/N]\t");
	char p = (char)getchar();
	if (p == 'Y' || p == 'y' || p == 'T' || p == 't')
	{
		dl_WriteLog("Invoking compiler...");
		system("gmake");
	}
	
	dl_WriteLog("Exiting program");
	dl_Deinit();
	return 0;
}
