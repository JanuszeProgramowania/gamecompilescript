#ifndef WORK_H_
#define WORK_H_
	#include <stdio.h>
	#include <string.h>
	
	void PrintVersion();
	void ShowWrongSyntaxInfo();
	void ShowHelp();
	
	int WriteVersionToFile(char* file, char* version, int use_template);
#endif
