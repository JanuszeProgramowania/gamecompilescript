/*
       _               ____              _                     _    _ 
      | |             |  _ \            | |                   | |  (_)
      | | __ _ _ __   | |_) | ___  _ __ | | _______      _____| | ___ 
  _   | |/ _` | '_ \  |  _ < / _ \| '_ \| |/ / _ \ \ /\ / / __| |/ / |
 | |__| | (_| | | | | | |_) | (_) | | | |   < (_) \ V  V /\__ \   <| |
  \____/ \__,_|_| |_| |____/ \___/|_| |_|_|\_\___/ \_/\_/ |___/_|\_\_|
*/


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "clock.h"

FILE* gDevLog = NULL;
char* gDevLogFileName = "gcs.log";
static int dl_Initialized = 0;
void dl_Init()
{
	gDevLog = fopen(gDevLogFileName, "w+");
	if (gDevLog == NULL)
		dl_Initialized = 0;
	else
		dl_Initialized = 1;
}

int dl_isInit()
{
	return dl_Initialized;
}

FILE* dl_GetFileHandle()
{
	return gDevLog;
}

void dl_WriteLog(const char* msg, ...)
{
	if (dl_isInit() == 0)
	{
		printf("[DEVLOG]\tCannot access %s file - handle broken (points to NULL)!\n", gDevLogFileName);
		return;
	}
	va_list arg;
	va_start(arg, msg);
	char buf[512];
	char timebuff[256];
	GetTime(timebuff);
	sprintf(buf, "%s\t%s\n", timebuff, msg);
    vfprintf(dl_GetFileHandle(), buf, arg);
    va_end(arg);
}

void dl_Deinit()
{
	fclose(dl_GetFileHandle());
}
