#ifndef __DEVLOG_H__
	#define __DEVLOG_H__
#endif
#include <stdio.h>
#include <stdlib.h>

int dl_Init();

int dl_isInit();

FILE* dl_GetFileHandle();

void dl_WriteLog(const char* msg, ...);

void dl_Deinit();
