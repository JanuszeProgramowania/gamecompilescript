#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int GitInstalled()
{
	char mangit[8192];
	FILE* fp = popen("man git", "r");
	if (fp == NULL)
	{
		printf("Failed to check if git is installed. Program will exit\n");
		dl_WriteLog("Failed to check if git is installed - command prompt handle was broken");
		exit(1);
	}
	while(fgets(mangit, sizeof(mangit)-1, fp) != NULL)
		if (strstr(mangit, "stupid") != NULL || strstr(mangit, "SYNOPSIS") != NULL)
		{
			dl_WriteLog("Success! Git is installed!");
			fclose(fp);
			return 1;
		}
		
	dl_WriteLog("Git was not found :(");
	return 0;
}

void LastRevision(char commit[], int r)
{
	FILE* fp = popen("cd $SRCPATH && git log -1 --pretty=%B | tr -d '\n' | tr ' ' '_'", "r");
	int i = 0;
	if (fp == NULL)
	{
		dl_WriteLog("Failed to check last revision of source code.");
		printf("Failed to check git last commit. Program will exit\n" );
		exit(1);
	}
	
	fgets(commit, r, fp);
	fclose(fp);
	
	for(i = 0; i < r; i++)
		if (commit[i] == '\n') commit[i] = '\0';
}
