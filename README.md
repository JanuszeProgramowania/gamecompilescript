# README #

Game Compile Script v1.0.0 alpha

### How do I get set up? ###
It should be easy to set everything up. You have to install gcc >= 4.8 and compile on the same system you use to compile game/db.
Remember to set environment variable SRCPATH to path where you have .git folder containing project data if you want to use -G option!

### Code review ###
* ** main.c** - entry point for an application. "Brain" of an app
* **work.c** - functions to change source revision in version.cpp
* **devlog.c** - problematic algorithm that can NOT be used until fixed
* **gitmanager.c** - manager that helps to integrate script with git utility