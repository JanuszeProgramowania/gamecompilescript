#ifndef __CLOCK_H__
#define __CLOCK_H__
#include <stdio.h>
#include <string.h>
#include <time.h>

void GetTime(char* buff)
{
	time_t rawtime;
	struct tm timeinfo;
	char timestring[128];
	
	time ( &rawtime );
	timeinfo = *localtime ( &rawtime );
	
    strftime(timestring, sizeof(timestring), "[%Y-%m-%d] %H:%M:%S", &timeinfo);
	if (strlen(timestring) > 0)
		strcpy(buff, timestring);
	else
		strcpy(buff, "UNKNOWN");
}
#endif
