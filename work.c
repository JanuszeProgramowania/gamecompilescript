#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <stddef.h>
#include "work.h"

#define BUFF_CAP 1<<15

	const char STRIP[]    = "+**************************************+";
	const char VERSION[]  = "GameCompScript v1.0.2 by JachuPL";
#ifdef BUILD_DATE
	const char BUILD[]	  = "  Last build timestamp: %d  ";
#endif
	const char USAGE1[]   = "  Usage: ./gcs [option] [parameter] ";
	const char USAGE2[]   = "   For options list, use ./gcs  -l  ";
	const char OPTIONSH[] = "Available options:              ";
	const char OPTIONS1[] = "\t-v\t\tshows current script version";
	const char OPTIONS2[] = "\t-c [file]\ttarget version file to change";
	const char OPTIONS3[] = "\t-l\t\tshows this list";
	const char OPTIONS4[] = "\t-t\t\tuse builtin template";
	const char OPTIONS5[] = "\t-G\t\tuif git is installed, uses last commit's title as revision";
	const char VERSION_CPP[] = "#include <stdio.h>\n\nvoid WriteVersion()\n{\n#ifndef __WIN32__\n	FILE* fp = fopen(\"VERSION.txt\", \"w\");\n\n	if (fp)\n	{\n		fprintf(fp, \"game perforce revision: %s\\n\");\n		fprintf(fp, \"%%s@%%s:%%s\", __USER__, __HOSTNAME__, __PWD__);\n		fclose(fp);\n	}\n#endif\n}\n\n";

char *replace_str2(const char *str, const char *old, const char *new);
void PrintVersion()
{
	printf("%s\n", STRIP);
	printf("| %s |\n", VERSION);
#ifdef BUILD_DATE
	char buff[256];
	sprintf(buff, BUILD, BUILD_DATE);
	printf("| %s |\n", buff);
#endif
	printf("| %s |\n", USAGE1);
	printf("| %s |\n", USAGE2);
	printf("%s\n", STRIP);
}

void ShowWrongSyntaxInfo()
{
	printf("Wrong syntax:\n");
	printf("%s\n", USAGE1);
	printf("%s\n", USAGE2);
}

void ShowHelp()
{
	printf("%s\n", OPTIONSH);
	printf("%s\n", OPTIONS1);
	printf("%s\n", OPTIONS2);
	printf("%s\n", OPTIONS3);
	printf("%s\n", OPTIONS4);
	printf("%s\n", OPTIONS5);
}

int WriteVersionToFile(char* file, char* version, int use_template)
{
	FILE *fp;
	
	char buff[BUFF_CAP];
	char result[BUFF_CAP];
	char* newVersion;
	int app = 0;
	
	dl_WriteLog("Writing version to file");
	
	if (use_template == 0)
	{
		dl_WriteLog("Writing revision to file selected by user");
		fp = fopen(file, "r");
		if (fp == NULL) return 999;
		fread(buff,sizeof(char),BUFF_CAP,fp);
		fclose(fp);
		
		sprintf(newVersion, "\"%s\"", version);
		sprintf(result, "\"%s\"", replace_str2(buff, "__P4_VERSION__", newVersion));
		fp = fopen(file, "w");
		fprintf(fp, "%s", result);
	}
	else
	{
		dl_WriteLog("Using built-in template");
		fp = fopen(file, "w");
		if (fp == NULL) return 999;
		app = sprintf(result, VERSION_CPP, version);
		fprintf(fp, "%s", result);
	}
	return fclose(fp);
}

//Credits to Laird Shaw, http://creativeandcritical.net/str-replace-c/
char *replace_str2(const char *str, const char *old, const char *new)
{
	char *ret, *r;
	const char *p, *q;
	size_t oldlen = strlen(old);
	size_t count, retlen, newlen = strlen(new);
	int samesize = (oldlen == newlen);

	if (!samesize) {
		for (count = 0, p = str; (q = strstr(p, old)) != NULL; p = q + oldlen)
			count++;
		retlen = p - str + strlen(p) + count * (newlen - oldlen);
	} else
		retlen = strlen(str);

	if ((ret = malloc(retlen + 1)) == NULL)
		return NULL;

	r = ret, p = str;
	while (1) {
		 /*
		if (!samesize && !count--)
			break;
		 */
		if ((q = strstr(p, old)) == NULL)
			break;
		ptrdiff_t l = q - p;
		memcpy(r, p, l);
		r += l;
		memcpy(r, new, newlen);
		r += newlen;
		p = q + oldlen;
	}
	strcpy(r, p);

	return ret;
}
